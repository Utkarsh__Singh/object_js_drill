
function keys(obj) {
    // Based on http://underscorejs.org/#keys
    resArr=[]
    // Retrieve all the names of the object's properties.
    for (key in obj) {
        resArr.push(key)
    }
    // Return the keys as strings in an array.
    return resArr
}
// console.log(keys(testObject))
module.exports = keys;