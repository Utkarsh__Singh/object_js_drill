// use this object to test your functions
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const pairs = require('../pairs')
let expectedOutput = [ [ 'name', 'Bruce Wayne' ], [ 'age', 36 ], [ 'location', 'Gotham' ] ]

if (JSON.stringify(expectedOutput)===JSON.stringify(pairs(testObject))) {
    console.log(`test passed`)
}
else {console.log(`test failed`)}