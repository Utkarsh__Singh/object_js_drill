const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const mapObject = require('../mapObjects')

let expectedOutput = {
  name: 'callback invoke for name',
  age: 'callback invoke for age',
  location: 'callback invoke for location'
}
// callback function
function cb(obj,key) {
  obj[key] = `callback invoke for ${key}`;
}
if (JSON.stringify(expectedOutput)===JSON.stringify(mapObject(testObject, cb))) {
  console.log(`test passed`)
}
else {console.log(`test failed`)}