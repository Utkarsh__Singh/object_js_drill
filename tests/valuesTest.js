const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
// use this object to test your functions
const values = require('../values')

let expectedOutput = Object.values(testObject)

if (JSON.stringify(expectedOutput) === JSON.stringify(values(testObject))) {
    console.log(`test passed`)
}
else {console.log(`test failed`)}