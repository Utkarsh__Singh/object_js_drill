const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function defaults(obj, defaultProps) {
    // !Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // *Return `obj`.
    // http://underscorejs.org/#defaults
    return ({...defaultProps, ...obj})

}
let defaultProps = { name: 'Utkarsh', age: "55", wealth: 'billionare' }
// driver code
function main() {
    return defaults(testObject, defaultProps)
}
// console.log(main())
module.exports = main();