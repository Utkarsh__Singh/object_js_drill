const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function mapObject(obj,cb) {
    
    // Like map for arrays, but for objects. 
    for (key in obj) {
        // Transform the value of each property in turn by passing it to the callback function.
        cb(obj,key);
    }
    return obj;
}

// console.log(
//     mapObject(testObject)
// )
module.exports = mapObject;