
function values(obj) {
    let resArr=[]
// Return all of the values of the object's own properties.
    for (key in obj) {
        resArr.push(obj[key]);
    }
    return resArr
    // Ignore functions
}

module.exports = values;