function pairs(obj) {
    // Convert an object into a list of [key, value] pairs.
    let keyValueList=[]
    for (key in obj) {
        keyValueList.push([key,obj[key]]);
    }
    return keyValueList
}
module.exports =pairs;