const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
function invert(obj) {
    // !Returns a copy of the object where the keys have become the values and the values the keys.
    let copyObj={};
    for (let [k, v] of Object.entries(obj)) {
        copyObj[v] = k
    }
    return copyObj;
}
// driver code
function main() {
    //* Assume that all of the object's values will be unique and string serializable.
    console.log(invert(testObject))
    return invert(testObject)
}

module.exports = main();